package aufgaben;

import internal.JavaKaraTestBase;
import org.junit.jupiter.api.Test;

/**
 * <h1>Kara, der Tunnelsucher II</h1>
 * <img src="../../resources/doc-files/tunnel_entrance-world.gif" />
 * <p>Kara will den Ausgang des Tunnels finden (Feld 2b). Dazu muss er zun&auml;chst den Tunnel durchqueren.
 * Schreiben Sie ein Programm, das ihn auf dem ersten Feld nach dem Tunnel anhalten l&auml;sst &ndash;
 * er soll nicht bis zum Ende der Gallerie laufen! </p>
 */
public class Aufgabe03TunnelAusgangTest extends JavaKaraTestBase {
	@Override
	protected void myProgram() {

	}

	@Test
	void world1() {
		testWorld(" ttttt   |e        |   tttttt|", x -> x.karaOn(6, 1));
	}

	@Test
	void world2() {
		testWorld("   tttttt|e        | ttttt   |", x -> x.karaOn(6, 1));
	}

	@Test
	void world3() {
		testWorld("   tt    |e        |   tt    |", x -> x.karaOn(5, 1));
	}
}
